# Allegro Credit Automated Tests via Postman / Newman

## Prequisites
* Node 10 (newman requirement)
* NPM
* Dependencies in the package.json  (newman, slack reporting)

## Installation

Run the dependencies saved in the package.json
```
npm install
```

## Running

Call the NPM module via npx
```
npx newman
```

## Envs

These envs are secret and must be set on the ENV or CI

POSTMAN_API_KEY
POSTMAN_SLACK_URL

## Vars

This vars are public information that can be changed.

$POSTMAN_COLLECTION_ID='11884905-eff77268-c9ca-44e5-9f03-a12b4cb148f2'
The collection to run.

$POSTMAN_ENVIRONMENT_ID='12652925-92af4884-80b4-4276-b433-75d6bdd989d3'
The environment variables to load.

$POSTMAN_SLACK_CHANNEL="#it-integration-tests"
The slack channel to post too (must be set in the webhook)


## Run the powershell

It downloads the collection from the specified id. 
It downloads the environment data bag from the specified id. 
It runs the collection and uploads it to slack.

```
./allegro-newman.ps1
```
