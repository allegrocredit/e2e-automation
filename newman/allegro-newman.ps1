$POSTMAN_COLLECTION_ID='11884905-eff77268-c9ca-44e5-9f03-a12b4cb148f2'
$POSTMAN_ENVIRONMENT_ID='12652925-92af4884-80b4-4276-b433-75d6bdd989d3'
$POSTMAN_SLACK_CHANNEL="#it-integration-tests"
$POSTMAN_API_KEY=$env:POSTMAN_API_KEY
$getCollectionCall = "https://api.getpostman.com/collections/" + $POSTMAN_COLLECTION_ID + "?apikey=$POSTMAN_API_KEY" 
$getEnvironmentCall= "https://api.getpostman.com/environments/" + $POSTMAN_ENVIRONMENT_ID + "?apikey=$POSTMAN_API_KEY"
npx -c "newman run $getCollectionCall --suppress-exit-code --insecure --delay-request 5000 -r slackmsg --reporter-slackmsg-webhookurl $env:POSTMAN_SLACK_URL --reporter-slackmsg-channel=$env:POSTMAN_SLACK_CHANNEL --environment $getEnvironmentCall"
