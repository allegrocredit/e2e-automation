package allegrocredit.app.merchantsignup;

import allegrocredit.app.BaseTest;
import allegrocredit.app.helper.Industry;
import allegrocredit.app.helper.Merchant;
import allegrocredit.app.helper.TestDataGenerator;
import allegrocredit.app.pageobject.AllegroCredit;
import org.testng.annotations.*;

public class MerchantSignupTests extends BaseTest {

    Merchant tryMerchant;

    @BeforeClass
    public void setUp() {
        tryMerchant = TestDataGenerator.generateNewMerchant();
        new AllegroCredit().openBrowser();
    }

    @AfterMethod
    public void trySigningUp() {
        new AllegroCredit()
                .merchantSignup(tryMerchant)
                .refresh();
    }

    @AfterClass
    public void tearDown() {
        new AllegroCredit().closeBrowser();
    }


    @Test
    public void tryIndustry_Hearing() {
        tryMerchant.setIndustry(Industry.HEARING);
    }

    @Test
    public void tryIndustry_Medical() {
        tryMerchant.setIndustry(Industry.MEDICAL);
    }

    @Test
    public void tryIndustry_Music() {
        tryMerchant.setIndustry(Industry.MUSIC);
    }

    @Test
    public void tryIndustry_Dental() {
        tryMerchant.setIndustry(Industry.DENTAL);
    }

}
