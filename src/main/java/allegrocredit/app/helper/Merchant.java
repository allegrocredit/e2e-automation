package allegrocredit.app.helper;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class Merchant {
    String company;
    Industry industry;
    String firstName;
    String lastName;
    String phone;
    String email;
    String userID;
    String password;
    String dealerID;
}
