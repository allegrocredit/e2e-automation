package allegrocredit.app.helper;

import java.util.Date;

public class TestDataGenerator {

    public static Merchant generateNewMerchant() {
        return new Merchant(
                String.format("Acme%s Company", TestDataGenerator.getUniqueId()),
                Industry.HEARING,
                "John",
                "Smith",
                "4085551111",
                "jsmith@acme.com",
                "90006.81",
                "CjousterKab1",
                "R2132"
        );
    }

    public static long getUniqueId() {
        return new Date().getTime();
    }

}
