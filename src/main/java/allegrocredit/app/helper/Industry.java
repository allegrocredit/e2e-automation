package allegrocredit.app.helper;

public enum Industry {
    HEARING ("industry_hearing"),
    MUSIC   ("industry_music"),
    DENTAL  ("industry_dental"),
    MEDICAL ("industry_medical");

    public final String id;
    Industry(String id) {
        this.id = id;
    }
}
