package allegrocredit.app.pageobject;

import allegrocredit.app.helper.Expected;
import allegrocredit.app.helper.Industry;
import allegrocredit.app.helper.Merchant;
import org.assertj.core.api.Assertions;
import org.assertj.core.api.SoftAssertions;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.assertj.core.api.Assertions.assertThat;

//public class Portal extends BasePageObject {
//
//    protected WebDriver driver;
//
//    public Portal(WebDriver driver) {
//        this.driver = driver;
//    }
//
//    public Portal openBrowser() {
//        driver.get("https://allegrocredittest.com/");
//        new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(By.linkText("Login")));
//        return new Portal(driver);
//    }
//
//    public void closeBrowser() {
//        driver.quit();
//    }
//
//    public Portal login(Merchant merchant) {
//        this
//                .clickNavbarLogin()
//                .enterUserID(merchant.getUserID())
//                .enterPassword(merchant.getPassword())
//                .clickLogin();
//
//        By byTitle = By.cssSelector(".modal.fade.in .modal-title");
//        new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(byTitle));
//        String actual = driver.findElement(byTitle).getText();
//
//        Assertions.assertThat(actual).as("Merchant Signup title incorrect" ).isEqualTo("Merchant Signup");
//        return this;
//    }
//
//    public Portal clickNavbarLogin() {
//        By byNavbarLogin = By.linkText("Login");
//        new WebDriverWait(driver, 30).until(ExpectedConditions.elementToBeClickable(byNavbarLogin)).click();
//        return this;
//    }
//
//    public Portal enterUserID(String expectedUserID) {
//        By byUserID = By.cssSelector("#username");
//        WebElement inputUserID = new WebDriverWait(driver, 30).until(ExpectedConditions.elementToBeClickable(byUserID));
//        inputUserID.clear();
//        inputUserID.sendKeys(expectedUserID);
//        return this;
//    }
//
//    public Portal enterPassword(String expectedPassword) {
//        By byPassword = By.cssSelector("#password");
//        WebElement inputPassword = new WebDriverWait(driver, 30).until(ExpectedConditions.elementToBeClickable(byPassword));
//        inputPassword.clear();
//        inputPassword.sendKeys(expectedPassword);
//        return this;
//    }
//
//    public Portal clickLogin() {
//        By byLogin = By.cssSelector("#submit-login");
//        new WebDriverWait(driver, 30).until(ExpectedConditions.elementToBeClickable(byLogin)).click();
//        return this;
//    }
//
//    public Portal impersonate(Merchant merchant) {
//        this
//                .clickImpersonate()
//                .selectDealerId()
//                .enterDealerId(merchant.getDealerID())
//                .clickSearch();
//        return this;
//    }
//
//    public Portal clickImpersonate() {
//        By byImpersonate = By.linkText("Impersonate");
//        new WebDriverWait(driver, 30).until(ExpectedConditions.elementToBeClickable(byImpersonate)).click();
//        return this;
//    }
//
//    public Portal merchantSignup(Merchant merchant) {
//        try {
//            this
//                .clickMerchantSignup()
//                .enterCompany(merchant.getCompany())
//                .selectIndustry(merchant.getIndustry())
//                .enterFirstName(merchant.getFirstName())
//                .enterLastName(merchant.getLastName())
//                .enterPhone(merchant.getPhone())
//                .enterEmail(merchant.getEmail())
//                .clickSubmit()
//                .verifySignupSubmission(Expected.SUCCESS);
//            }
//            catch(ElementClickInterceptedException ecie) {
//                ecie.printStackTrace();
//            }
//        return this;
//    }
//
//    public Portal clickMerchantSignup() {
//        new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(By.linkText("Merchant Signup")));
//        driver.findElement(By.linkText("Merchant Signup")).click();
//
//        By byTitle = By.cssSelector(".modal.fade.in .modal-title");
//        new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(byTitle));
//        String actual = driver.findElement(byTitle).getText();
//
//        Assertions.assertThat(actual).as("Merchant Signup title incorrect" ).isEqualTo("Merchant Signup");
//        driver.switchTo().frame(0);
//        return this;
//    }
//
//    public Portal enterCompany(String expectedCompany) {
//        By byCompany = By.cssSelector("#embed-merchant-signup #company_legal");
//        WebElement inputCompany = new WebDriverWait(driver, 30).until(ExpectedConditions.elementToBeClickable(byCompany));
//        inputCompany.clear();
//        inputCompany.sendKeys(expectedCompany);
//        return this;
//    }
//
//    public Portal selectIndustry(Industry industry) {
//        By byRadio = By.cssSelector("#embed-merchant-signup label[for='" + industry.id + "']");
//        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOfElementLocated(byRadio)).click();
//        return this;
//    }
//
//    public Portal enterFirstName(String expectedFirstName) {
//        By byFirstName = By.cssSelector("#embed-merchant-signup #contact_first_name");
//        WebElement inputFirstName = new WebDriverWait(driver, 30).until(ExpectedConditions.elementToBeClickable(byFirstName));
//        inputFirstName.clear();
//        inputFirstName.sendKeys(expectedFirstName);
//        return this;
//    }
//
//    public Portal enterLastName(String expectedLastName) {
//        By byLastName = By.cssSelector("#embed-merchant-signup #contact_last_name");
//        WebElement inputLastName = new WebDriverWait(driver, 30).until(ExpectedConditions.elementToBeClickable(byLastName));
//        inputLastName.clear();
//        inputLastName.sendKeys(expectedLastName + Keys.TAB);
//        return this;
//    }
//
//    public Portal enterPhone(String expectedPhone) {
//        By byPhone = By.cssSelector("#embed-merchant-signup #phone");
//        new WebDriverWait(driver, 30).until(ExpectedConditions.elementToBeClickable(byPhone)).sendKeys(expectedPhone);
//        return this;
//    }
//
//    public Portal enterEmail(String expectedEmail) {
//        By byEmail = By.cssSelector("#embed-merchant-signup #email");
//        WebElement inputEmail = new WebDriverWait(driver, 30).until(ExpectedConditions.elementToBeClickable(byEmail));
//        inputEmail.clear();
//        inputEmail.sendKeys(expectedEmail);
//        return this;
//    }
//
//    public Portal clickSubmit() {
//        try {
//            By bySubmit = By.cssSelector("#embed-merchant-signup #submit-submit");
//            new WebDriverWait(driver, 30).until(ExpectedConditions.elementToBeClickable(bySubmit)).click();
//        }
//        catch(ElementClickInterceptedException e) {
//            SoftAssertions.assertSoftly(softly -> {
//                softly.assertThat(false).as("Encountered ElementClickInterceptedException again").isTrue();
//            });
//        }
//        return this;
//    }
//
//    public Portal verifySignupSubmission(Expected expected) {
//        By byAlert = By.cssSelector("#embed-merchant-signup .alert-info");
//        String actualMessage = new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(byAlert)).getText();
//
//        assertThat(actualMessage).as("Merchant Signup success alert message incorrect" ).isEqualTo(expected.message);
//        return this;
//    }
//
//    private void snooze(long secs) {
//        long inSeconds = secs * 1000;
//        try {
//            Thread.sleep(inSeconds);
//        }
//        catch(InterruptedException ie) {
//            throw new TimeoutException("Timed out after " + inSeconds + " secs");
//        }
//    }
//
//}
