package allegrocredit.app.pageobject;

import allegrocredit.app.helper.Expected;
import allegrocredit.app.helper.Merchant;
import org.assertj.core.api.Assertions;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AllegroCredit extends BasePageObject {

    public AllegroCredit openBrowser() {
        super.createDriver();
        driver.get("https://allegrocredittest.com/");
        new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(By.linkText("Login")));
        return this;
    }

    public void refresh() {
        driver.navigate().refresh();
    }

    public void closeBrowser() {
        driver.quit();
    }

    public AllegroCredit merchantSignup(Merchant merchant) {
        try {
            new MerchantSignup()
                    .clickMerchantSignup()
                    .enterCompany(merchant.getCompany())
                    .selectIndustry(merchant.getIndustry())
                    .enterFirstName(merchant.getFirstName())
                    .enterLastName(merchant.getLastName())
                    .enterPhone(merchant.getPhone())
                    .enterEmail(merchant.getEmail())
                    .clickSubmit()
                    .verifySignupSubmission(Expected.SUCCESS)
                    .clickDone();
        }
        catch(ElementClickInterceptedException ecie) {
            ecie.printStackTrace();
        }
        return this;
    }

//    public AllegroCredit login(Merchant merchant) {
//        this
//                .clickNavbarLogin()
//                .enterUserID(merchant.getUserID())
//                .enterPassword(merchant.getPassword())
//                .clickLogin();
//
//        By byTitle = By.cssSelector(".modal.fade.in .modal-title");
//        new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(byTitle));
//        String actual = driver.findElement(byTitle).getText();
//
//        Assertions.assertThat(actual).as("Merchant Signup title incorrect" ).isEqualTo("Merchant Signup");
//        return this;
//    }
//
//    public AllegroCredit impersonate(Merchant merchant) {
//        this
//                .clickImpersonate()
//                .selectDealerId()
//                .enterDealerId(merchant.getDealerID())
//                .clickSearch();
//        return this;
//    }

}
