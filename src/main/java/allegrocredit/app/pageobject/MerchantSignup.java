package allegrocredit.app.pageobject;

import allegrocredit.app.helper.Expected;
import allegrocredit.app.helper.Industry;
import org.assertj.core.api.Assertions;
import org.assertj.core.api.SoftAssertions;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.assertj.core.api.Assertions.assertThat;

public class MerchantSignup extends BasePageObject {

    public MerchantSignup clickMerchantSignup() {
        new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(By.linkText("Merchant Signup")));
        driver.findElement(By.linkText("Merchant Signup")).click();

        By byTitle = By.cssSelector(".modal.fade.in .modal-title");
        new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(byTitle));
        String actual = driver.findElement(byTitle).getText();

        Assertions.assertThat(actual).as("Merchant Signup title incorrect" ).isEqualTo("Merchant Signup");
        driver.switchTo().frame(0);
        return this;
    }

    public MerchantSignup enterCompany(String expectedCompany) {
        By byCompany = By.cssSelector("#embed-merchant-signup #company_legal");
        WebElement inputCompany = new WebDriverWait(driver, 30).until(ExpectedConditions.elementToBeClickable(byCompany));
        inputCompany.clear();
        inputCompany.sendKeys(expectedCompany);
        return this;
    }

    public MerchantSignup selectIndustry(Industry industry) {
        // TODO: find out why we need to add a delay here
        snooze(2);
        By byRadio = By.cssSelector("#embed-merchant-signup label[for='" + industry.id + "']");
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOfElementLocated(byRadio)).click();
        return this;
    }

    public MerchantSignup enterFirstName(String expectedFirstName) {
        By byFirstName = By.cssSelector("#embed-merchant-signup #contact_first_name");
        WebElement inputFirstName = new WebDriverWait(driver, 30).until(ExpectedConditions.elementToBeClickable(byFirstName));
        inputFirstName.clear();
        inputFirstName.sendKeys(expectedFirstName);
        return this;
    }

    public MerchantSignup enterLastName(String expectedLastName) {
        By byLastName = By.cssSelector("#embed-merchant-signup #contact_last_name");
        WebElement inputLastName = new WebDriverWait(driver, 30).until(ExpectedConditions.elementToBeClickable(byLastName));
        inputLastName.clear();
        inputLastName.sendKeys(expectedLastName + Keys.TAB);
        return this;
    }

    public MerchantSignup enterPhone(String expectedPhone) {
        By byPhone = By.cssSelector("#embed-merchant-signup #phone");
        new WebDriverWait(driver, 30).until(ExpectedConditions.elementToBeClickable(byPhone)).sendKeys(expectedPhone);
        return this;
    }

    public MerchantSignup enterEmail(String expectedEmail) {
        By byEmail = By.cssSelector("#embed-merchant-signup #email");
        WebElement inputEmail = new WebDriverWait(driver, 30).until(ExpectedConditions.elementToBeClickable(byEmail));
        inputEmail.clear();
        inputEmail.sendKeys(expectedEmail);
        return this;
    }

    public MerchantSignup clickSubmit() {
        try {
            By bySubmit = By.cssSelector("#embed-merchant-signup #submit-submit");
            new WebDriverWait(driver, 30).until(ExpectedConditions.elementToBeClickable(bySubmit)).click();
        }
        catch(ElementClickInterceptedException e) {
            SoftAssertions.assertSoftly(softly -> {
                softly.assertThat(false).as("Encountered ElementClickInterceptedException again").isTrue();
            });
        }
        return this;
    }

    public MerchantSignup clickDone() {
        By byDone = By.cssSelector("#submit-done");
        new WebDriverWait(driver, 30).until(ExpectedConditions.elementToBeClickable(byDone)).click();
        return this;
    }

    public MerchantSignup verifySignupSubmission(Expected expected) {
        By byAlert = By.cssSelector("#embed-merchant-signup .alert-info");
        String actualMessage = new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(byAlert)).getText();

        assertThat(actualMessage).as("Merchant Signup success alert message incorrect" ).isEqualTo(expected.message);
        return this;
    }

}
